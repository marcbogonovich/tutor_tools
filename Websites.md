# YouTubers

[North02](https://www.youtube.com/@NORTH02/videos)

[Paleo Analysis](https://www.youtube.com/@PaleoAnalysis/videos)

[Moth Light Media](https://www.youtube.com/@mothlightmedia1936/videos)

[3 Blue 1 Brown](https://www.youtube.com/@3blue1brown)

[Combo Class](https://www.youtube.com/@ComboClass)

[Domotro from Combo Class](https://www.youtube.com/@Domotro)

### Investment YouTubers

[Patrick Boyle](https://www.youtube.com/@PBoyle)

[Asionometry](https://www.youtube.com/@Asianometry)

# Websites

## Book websites

[Monkeypen](https://monkeypen.com/pages/free-childrens-books) free books.

## Quiz Websites
[Jetpunk](https://www.jetpunk.com)
- e.g. [Every country shape](https://www.jetpunk.com/user-quizzes/6121/every-country-shape)

[Sporcle](https://www.sporcle.com)

[China provinces](https://www.sporcle.com/games/1447/prc_provinces)

## Learning apps

[Wordscapes](https://en.wikipedia.org/wiki/Wordscapes)

## Coding tutorial websites

[W3Schools](https://www.w3schools.com/)
- [HTML](https://www.w3schools.com/html/)
- [CSS](https://www.w3schools.com/css/)
- [Javascript](https://www.w3schools.com/js/)

NetworkChuck - [60 Linux Commands](https://www.youtube.com/watch?v=gd7BXuUQ91w)

## Math websites

[Calculator soup](https://www.calculatorsoup.com/)

[Simple addition maze puzzle](https://www.ducksters.com/games/numbermaze.php)

## Language learning

[Calculate IELTS band score](https://www.ielts.org/for-organisations/ielts-scoring-in-detail)

[FrequencyList.com](https://frequencylist.com/)

[Language learning Discord group](https://discord.com/channels/363985050578190336/423813514394861579)

[British Council](https://learnenglish.britishcouncil.org/grammar/english-grammar-reference/past-continuous)

[Seonaid's English Grammar](https://www.perfect-english-grammar.com/preposition-exercises.html). This link is for prepositions.

[100 most common English verbs](https://www.linguasorb.com/english/verbs/most-common-verbs/1). This highlights irregular verbs in red.

Discourse markers & Linking words: [1](https://www.writinglab.io/ielts-linking-words/) [2](https://warwick.ac.uk/fac/soc/al/globalpad-rip/openhouse/academicenglishskills/grammar/discourse/)

Contains exercises including on [Gerunds and Infinitives](https://www.englishpage.com/)

[Language Songs—Five year olds](https://www.youtube.com/watch?v=lWhqORImND0)


### Grammar

[Grammarly](https://app.grammarly.com/)

[GrammarCheck](https://www.grammarcheck.net/editor/)

[QuillBot](https://quillbot.com/grammar-check)

[Etymology](https://www.etymonline.com/search?q=best)

[Thesaurus]()

[Dictionary]()


## Investing

### Hong Kong

[Fidelity](https://www.fidelity.com.hk/en)
[HSBC—Investments](https://www.hsbc.com.hk/en-hk/investments/)

### General

[TD Ameritrade](https://www.tdameritrade.com/)
[Charles Schwab](https://www.schwab.com/)
