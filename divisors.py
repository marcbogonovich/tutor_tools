#!/usr/bin/python

#This script explores divisors.


#https://stackoverflow.com/questions/171765/what-is-the-best-way-to-get-all-the-divisors-of-a-number#171784
def divisorGenerator(n):
    for i in range(1,int(n/2+1)):
        if n%i == 0: yield i
    yield n
#Test divisor generator:
print(list(divisorGenerator(420)))


#h is the highest number of divisors yet
h = 0
for i in range(1, 10001):
    pass



#Here I will get the highest number of divisors of a number.
#Let me explain with examples:
#A prime number n has a value 1: 1 × n (excludes 1)
#6 has a value of 2:             1 × 2 × 3
#12 has a value of 3:            1 × 2 × 3 × 2
#I think this may be equal to the sum of the exponents of the prime factorization.
def sum_of_prime_factor_exponents(n):
    if n <= 1:
        return 0
    sum_of_exponents = 0
    factor = 2
    while factor * factor <= n:
        exponent = 0
        while n % factor == 0:
            exponent += 1
            n //= factor
        sum_of_exponents += exponent
        factor += 1
    if n > 1:
        sum_of_exponents += 1
    return sum_of_exponents

# Test the function
test_numbers = [12, 24, 18, 28, 100, 1000]
for num in test_numbers:
    result = sum_of_prime_factor_exponents(num)
    print(f"Sum of exponents in prime factorization of {num}: {result}")
for i in range(17001):
    s = sum_of_prime_factor_exponents(i)
    if s > 9:
        print(str(i)+':'+str(s))#, end=",")






















