#!/usr/bin/python3

# The purpose of this code is to estimate the vocabulary size of English students
# in 1:1 tutoring sessions. The code works in a linux terminal in the following manner.

# 1) Ingest a complete English lexicon file, turn the contents into a list.
# 2) Randomly spits out elements from the list (words) to the terminal (while also 
#    showing how many words have already been presented). And also present a stdin
#    to the user.
# 3) The user is instructed to enter a code based on their knowledge or lack of 
#    knowledge of the word. The particular code choice is based on the judgement of
#    the tutor and the student. 
# 4) The code is as follows.
#      0 - The student does not know the word.
#      1 - The student is able to reason out the word (using etymology, prefixes etc)
#          or partially knows the word.
#      2 - The student knows the word.
#      f - The student and tutor wish to end the vocabulary estimator (only after 
#          sufficient samples have been taken).
# 5) Receive the coded input from the user and store it in a list
# 6) When "f" is typed, first request an answer to the last word. And then:
# 7) Calculate the percent of words known and percent unknown. And calculate error range 
#    for the percent known. Calculate an estimate of the total vocabulary size by multiplying
#    the 'known' percentage with the total number of words in the lexicon and calculate 
#    confidence intervals.
# 8) Present these statistics back to the student and tutor.
# 9) Optionally, script restuls data can be placed into /vocab_estimator_RunReports.
#    This would perhaps require more code to document the name of the student (stdin)
#    and the time the script was executed.

# The English word list comes from:
# https://github.com/dwyl/english-words
# And the specific word list is called words_alpha.txt, which contains no numbers
# or symbols:
# https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt


# LIBRARIES
#===================================================================
import time
import random
import math
from tutor_functions import terminal_font

#TEST
#print(terminal_font("Bold green font, white background", fontcolor="white", bgcolor="green",
#	bold=True))
#END TEST

# INGEST THE LEXICON FILE & SEND THE CONTENTS TO A LIST
#===================================================================
# Opening the first N lines of a file:
# https://stackoverflow.com/questions/1767513/how-to-read-first-n-lines-of-a-file#1767589 
# Use the in-built strip function to remove leading and trailing space or newline characters
# from a string:
# https://www.geeksforgeeks.org/python-removing-newline-character-from-string/
#TEST
# One method for cycling through the lines:
#N = 10
#with open("words_alpha.txt") as words_alpha:
#    head = [next(words_alpha).strip() for x in range(N)]
#print(head)
#END TEST
#---------------------------------------------
# Simple method for cycling through lines:
# https://www.geeksforgeeks.org/read-a-file-line-by-line-in-python/
# Opening file
words_alpha = open('words_alpha.txt', 'r')
words = []
#TEST
#count = 0
#END TEST
# Using for loop
for line in words_alpha:
	words.append(line.strip())
	#TEST
	#print(len(words))
	#print(words[count])
	#count += 1
	#END TEST
# Closing files
words_alpha.close()

#TEST
#test = words[301000:301115]
#print(test)
#END TEST


# SHOW INSTRUCTIONS
#===================================================================
# Different methods to collect stdin in Python:
# https://www.geeksforgeeks.org/take-input-from-stdin-in-python/
print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
name = input("Student name : ")
age  = str(input("Student age  : "))
time = int(time.time())
#TEST
#print(name)
#print(age)
#print(time)
#END TEST

# Outputing colored text in Linux terminal:
# https://www.tutorialspoint.com/how-to-output-colored-text-to-a-linux-terminal
#TEST
#print("\033[1;31mThis is bold red text\033[0m")
#END TEST
print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
#OLD LINE
#print("\033[1;33mInstructions:\033[0m")
#END OLD LINE
print(terminal_font("Instructions: ", fontcolor="yellow", bold=True))
print("\nYou will be presented with one word at a time. These words \nare selected from a " + terminal_font("comprehensive list of English words", fontcolor="red", bold=True) + ".\nYou will then be asked to enter whether you know the word \nor do not know the word using the following code:\n\nYou don't know the word: \t\t\ttype 0\nYou know the word: \t\t\t\ttype 1\nYou are able to reason out the word's meaning: \ttype 2\n")
print("\n\n\n\n\n\n\n\n")
any_Key = input("Type any key to begin: ")


# INITIATE TESTING VARIABLES
#===================================================================
code = ""
words_max_index = len(words)-1
test_count = 0
known      = 0
unknown    = 0
partial    = 0

#TEST
#rand_index = random.randint(0, words_max_index)
#print(words[rand_index])
#print(words[random.randint(0, words_max_index)])
#print(words[random.randint(0, words_max_index)])
#print(words[random.randint(0, words_max_index)])
#print(words_max_index)
#END TEST


# BEGIN SHOWING WORDS
#===================================================================
while code != "f":
	rand_index = random.randint(0, words_max_index)
	print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
	print("Code:\nDon't know:\t0\nKnow:\t\t1\nPartially know:\t2\nFinish:\t\tf\n")
	print("score: ")
	print("\033[1;32m", known, "/", known+unknown+partial, " known", "\033[0m")
	print("\033[1;32m", partial, "/", known+unknown+partial, " Partially known", "\033[0m")
	print("\n\n\n\n")
	print("Do you know: \033[1;33m",words[rand_index],"\033[0m")
	print("\n\n\n\n\n\n")
	code = str(input("Answer: "))
	while code != "0" and code != "1" and code != "2" and code != "f":
		print("\033[1;33mYou need to enter 0, 1, 2, or f\033[0m")
		code = str(input("Try again: "))
	if code == "0":
		unknown = unknown + 1
	elif code == "1":
		known = known + 1
	elif code == "2":
		partial = partial + 1
	elif code == "f":
		#Collect an answer on the last word
		print("Well, do you know \033[1;33m",words[rand_index],"\033[0m")
		finalcode = str(input("Answer: "))
		while finalcode != "0" and finalcode != "1" and finalcode != "2":
			print("\033[1;33mYou need to enter 0, 1, or 2\033[0m")
			finalcode = str(input("Try again: "))
			if finalcode == "0":
				unknown = unknown + 1
			elif finalcode == "1":
				known = known + 1
			elif finalcode == "2":
				partial = partial + 1
	else:
		print("Something went wrong, shouldn't be here, lmao")


# CALCULATE FINAL STATISTICS
#===================================================================
# Calculate confidence interval for a proportion:
# https://www.statology.org/confidence-interval-proportion/
# Confidence Interval = p  +/-  z*√p(1-p) / n
# where:
#    p: sample proportion
#    z: the chosen z-value
#    n: sample size
#Confidence Level	z-value
#0.90				1.645
#0.95				1.96
#0.99				2.58
#Calculate known statistics
n       = unknown+known+partial
p       = known/(unknown+known+partial)
p_conf_interval = 1.645 * math.sqrt(p*(1-p)/n)
p_upper = p + p_conf_interval
p_lower = p - p_conf_interval
#Calculate 'partially known' (p2) statistics
p2      = partial/(unknown+known+partial)
p2_conf_interval = 1.645 * math.sqrt(p2*(1-p2)/n)
p2_upper = p2 + p2_conf_interval
p2_lower = p2 - p2_conf_interval
#Calculate vocab size
vocab_size = p*len(words)
vocab_size2 = (p+p2)*len(words)


# PRINT FINAL STATISTICS
#===================================================================
print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
print("\033[1;33mKnown:           \033[0m",known)
print("\033[1;33mPartially known: \033[0m",partial)
print("\033[1;33mUnknown:         \033[0m",unknown, "\n")
print("\033[1;33mProportion known +/- 90percent confidence interval:\033[0m\n", p, " (", p_lower, "–", p_upper, ")")
print("\033[1;33mProportion partially known +/- 90percent confidence interval:\033[0m\n", p2, " (", p2_lower, "–", p2_upper, ")\n")
print("\033[1;32mTotal Words:\033[0m", len(words))
print("\033[1;32mEstimated Narrow Vocabulary Size: \033[0m", p, "×", len(words), "=", vocab_size)
print("\033[1;32mEstimated Broad Vocabulary Size:  \033[0m", p+p2, "×", len(words), "=", vocab_size2)
print("\n\n\n\n\n\n")


# EXPORT STATISTICS TO RunReport
#===================================================================
#name,age,time,known,partially_known,unknown,vocab_size,vocab_size2
with open("RunReports/RunReport.txt", 'a') as RunReport:
	RunReport.write(str(name) + "," + str(age) + "," + str(time) + "," + str(known) + "," + str(partial) + "," + str(unknown) + "," + str(vocab_size) + "," + str(vocab_size2) + "\n")









