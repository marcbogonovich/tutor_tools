#!/usr/bin/python

#This code will look through all words in words_alpha.txt and it will create 
#a subset of this list that are only n letters long.
#The code will then show one of these words at a time to you.


# LIBRARIES
#===================================================================
import time
import random
import math
from tutor_functions import terminal_font


# INGESTING words_alpha.txt
#===================================================================
words_alpha = open('words_alpha.txt', 'r')
words = []
for line in words_alpha:
	words.append(line.strip())
words_alpha.close()


# CREATING A SUBSET OF words WHERE WORDS HAVE n LETTERS
#===================================================================
#https://chatgpt.com/c/58913ca0-e7a3-4f6f-87c8-70d17f091612
n = 6
def words_with_n_letters(words, n):
    return [word for word in words if len(word) == n]

# Example usage:
#words = ["apple", "banana", "cherry", "date", "fig", "grape"]
n = 3
result = words_with_n_letters(words, n)
print(result)  # Output will be: ['apple', 'grape']


# PICK A WORD AT RANDOM, SHOW IT TO US
#===================================================================
while True:
	input("Press Enter to get a random word:")
	print(random.choice(result))






















