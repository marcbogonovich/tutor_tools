#!/usr/bin/python

#This is a game where the user guesses which fraction is larger... endlessly

import pygame
import random
import sys

# Initialize Pygame
pygame.init()

# Screen settings
SCREEN_WIDTH = 1500
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Fraction Comparison Game")

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)

# Fonts
font_large = pygame.font.Font(None, 72)
font_medium = pygame.font.Font(None, 48)
font_small = pygame.font.Font(None, 36)

# Game settings
BOX_WIDTH = 300
BOX_HEIGHT = 400
LEFT_BOX = pygame.Rect(50, 150, BOX_WIDTH, BOX_HEIGHT)
RIGHT_BOX = pygame.Rect(450, 150, BOX_WIDTH, BOX_HEIGHT)
FEEDBACK_DURATION = 1000  # milliseconds

def generate_fractions():
    """Generate two fractions with different denominators that are close in value"""
    while True:
        num1 = random.randint(1, 100)
        den1 = random.randint(1, 100)
        num2 = random.randint(1, 100)
        den2 = random.randint(1, 100)
        
        if den1 == den2:
            continue  # Ensure different denominators
        
        value1 = num1 / den1
        value2 = num2 / den2
        
        # Check if values are close but not equal
        if abs(value1 - value2) < 0.2 and value1 != value2:
            return (num1, den1), (num2, den2), 'left' if value1 > value2 else 'right'

def draw_fraction(fraction, position, color=BLACK):
    """Draw a fraction in the specified position"""
    num, den = fraction
    text = font_large.render(f"{num}/{den}", True, color)
    text_rect = text.get_rect(center=position)
    screen.blit(text, text_rect)

def main():
    running = True
    correct = 0
    total = 0
    feedback = None
    feedback_time = 0

    # Generate initial fractions
    left_frac, right_frac, correct_side = generate_fractions()

    while running:
        current_time = pygame.time.get_ticks()
        screen.fill(WHITE)

        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN and not feedback:
                mouse_pos = pygame.mouse.get_pos()
                if LEFT_BOX.collidepoint(mouse_pos):
                    total += 1
                    if correct_side == 'left':
                        correct += 1
                        feedback = ('correct', 'left')
                    else:
                        feedback = ('wrong', 'left')
                    feedback_time = current_time
                elif RIGHT_BOX.collidepoint(mouse_pos):
                    total += 1
                    if correct_side == 'right':
                        correct += 1
                        feedback = ('correct', 'right')
                    else:
                        feedback = ('wrong', 'right')
                    feedback_time = current_time

        # Draw score
        win_rate = correct / total * 100 if total > 0 else 0
        score_text = font_small.render(
            f"Correct: {correct}/{total} ({win_rate:.1f}%)", True, BLACK)
        screen.blit(score_text, (20, 20))

        # Draw fraction boxes
        left_color = BLUE
        right_color = BLUE
        
        # Apply feedback colors
        if feedback:
            result, side = feedback
            if result == 'correct':
                if side == 'left':
                    left_color = GREEN
                else:
                    right_color = GREEN
            else:
                if side == 'left':
                    left_color = RED
                    right_color = GREEN if correct_side == 'right' else RED
                else:
                    right_color = RED
                    left_color = GREEN if correct_side == 'left' else RED

        pygame.draw.rect(screen, left_color, LEFT_BOX, 3)
        pygame.draw.rect(screen, right_color, RIGHT_BOX, 3)

        # Draw fractions
        draw_fraction(left_frac, LEFT_BOX.center)
        draw_fraction(right_frac, RIGHT_BOX.center)

        # Handle feedback timeout
        if feedback and current_time - feedback_time > FEEDBACK_DURATION:
            feedback = None
            left_frac, right_frac, correct_side = generate_fractions()

        pygame.display.flip()
        pygame.time.Clock().tick(30)

    pygame.quit()
    sys.exit()

if __name__ == "__main__":
    main()
