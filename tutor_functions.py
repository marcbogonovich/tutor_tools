#!/usr/bin/python3

# The following are functions that will be used by tutor_tools

# TERMINAL COLORING FUNCTIONS
#===================================================================
# Here I am defining a function that will color text outputted to the terminal.
# There are three types of functions designs, I. II. and III., in declining 
# degree of design sophistication.
# https://www.tutorialspoint.com/how-to-output-colored-text-to-a-linux-terminal

# I. This function aims to obtain a wider combinatorial range of outputs
# color options are white, cyan, purple (or magenta), blue, yellow, green, red, black
def terminal_font(string, fontcolor="none", bgcolor="none", bold=False, underlined=False):
	fontstring = "\033["
	#Only needed if bold is true
	if bold==True:
		fontstring = fontstring + ";1"
	#Only needed if underlined is True
	if underlined==True:
		fontstring = fontstring + ";4"
	#bg color conditionals (nothing needed for "none")
	# white,cyan,purple (or magenta),blue,yellow,green,red,black
	if bgcolor=="white":
		fontstring = fontstring + ";47"
	if bgcolor=="cyan":
		fontstring = fontstring + ";46"
	if bgcolor=="purple":
		fontstring = fontstring + ";45"
	if bgcolor=="magenta":
		fontstring = fontstring + ";45"
	if bgcolor=="blue":
		fontstring = fontstring + ";44"
	if bgcolor=="yellow":
		fontstring = fontstring + ";43"
	if bgcolor=="green":
		fontstring = fontstring + ";42"
	if bgcolor=="red":
		fontstring = fontstring + ";41"
	if bgcolor=="black":
		fontstring = fontstring + ";40"
	#Font color conditionals
	# white,cyan,purple (or magenta),blue,yellow,green,red,black
	if fontcolor=="white":
		fontstring = fontstring + ";37"
	if fontcolor=="cyan":
		fontstring = fontstring + ";36"
	if fontcolor=="purple":
		fontstring = fontstring + ";35"
	if fontcolor=="magenta":
		fontstring = fontstring + ";35"
	if fontcolor=="blue":
		fontstring = fontstring + ";34"
	if fontcolor=="yellow":
		fontstring = fontstring + ";33"
	if fontcolor=="green":
		fontstring = fontstring + ";32"
	if fontcolor=="red":
		fontstring = fontstring + ";31"
	if fontcolor=="black":
		fontstring = fontstring + ";30"
	fontstring = fontstring + "m" + string + "\033[0m"
	return fontstring


# II. This function will be constructed with a switch statement, and will only contain
# 'bold' style and 'not bold' style.
def terminal_color(string, color="white", bold=True):
# white,cyan,purple (or magenta),blue,yellow,green,red,black
# purple and magenta yield the same result
	# Bold colors
	if color=="white"     and bold==True:
		return "\033[1;37m" + string + "\033[0m" 
	elif color=="cyan"    and bold==True:
		return "\033[1;36m" + string + "\033[0m" 
	elif color=="purple"  and bold==True:
		return "\033[1;35m" + string + "\033[0m"
	elif color=="magenta" and bold==True:
		return "\033[1;35m" + string + "\033[0m"
	elif color=="blue"    and bold==True:
		return "\033[1;34m" + string + "\033[0m"
	elif color=="yellow"  and bold==True:
		return "\033[1;33m" + string + "\033[0m"
	elif color=="green"   and bold==True:
		return "\033[1;32m" + string + "\033[0m"
	elif color=="red"     and bold==True:
		return "\033[1;31m" + string + "\033[0m"
	elif color=="black"   and bold==True:
		return "\033[1;30m" + string + "\033[0m"
	# Not bold colors
	elif color=="white"   and bold==False:
		return "\033[;37m" + string + "\033[0m" 
	elif color=="cyan"    and bold==False:
		return "\033[;36m" + string + "\033[0m" 
	elif color=="purple"  and bold==False:
		return "\033[;35m" + string + "\033[0m"
	elif color=="magenta" and bold==False:
		return "\033[;35m" + string + "\033[0m"
	elif color=="blue"    and bold==False:
		return "\033[;34m" + string + "\033[0m"
	elif color=="yellow"  and bold==False:
		return "\033[;33m" + string + "\033[0m"
	elif color=="green"   and bold==False:
		return "\033[;32m" + string + "\033[0m"
	elif color=="red"     and bold==False:
		return "\033[;31m" + string + "\033[0m"
	elif color=="black"   and bold==False:
		return "\033[;30m" + string + "\033[0m"
	else:
		print("error, check your input parameters")


# III. These are 'one shot' or 'one style' functions
# Experimenting with the codes
def terminal_color_experimenting(string):
	return "\033[;1;4;31;43m" + string + "\033[0m"
# Bold color functions
def terminal_color_white_bold(string):
	return "\033[1;37m" + string + "\033[0m"
def terminal_color_cyan_bold(string):
	return "\033[1;36m" + string + "\033[0m"
def terminal_color_purple_bold(string):
	return "\033[1;35m" + string + "\033[0m"
def terminal_color_blue_bold(string):
	return "\033[1;34m" + string + "\033[0m"
def terminal_color_yellow_bold(string):
	return "\033[1;33m" + string + "\033[0m"
def terminal_color_green_bold(string):
	return "\033[1;32m" + string + "\033[0m"
def terminal_color_red_bold(string):
	return "\033[1;31m" + string + "\033[0m"
def terminal_color_black_bold(string):
	return "\033[1;30m" + string + "\033[0m"

'''
# III. Testing the individual terminal coloring functions
#---------------------------------------------
# Experimenting with the codes
mystring = "mystring"
mystring = terminal_color_experimenting(mystring)
print(mystring)
# Bold colors
mystring = "mystring"
mystring = terminal_color_white_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_cyan_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_purple_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_blue_bold(mystring)
print(mystring)
mystring = "mystring"
print(mystring)
mystring = terminal_color_yellow_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_green_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_red_bold(mystring)
print(mystring)
mystring = "mystring"
mystring = terminal_color_black_bold(mystring)
print(mystring)


# II. Testing the terminal_color() function
#---------------------------------------------
# Default values
print(terminal_color("The terminal_color() function."))
# Different color values with bold
# white,cyan,purple (or magenta),blue,yellow,green,red,black
print(terminal_color("The terminal_color() function.", color="white",   bold=True))
print(terminal_color("The terminal_color() function.", color="cyan")) #testing default value again
print(terminal_color("The terminal_color() function.", color="purple",  bold=True))
print(terminal_color("The terminal_color() function.", color="magenta", bold=True))
print(terminal_color("The terminal_color() function.", color="blue",    bold=True))
print(terminal_color("The terminal_color() function.", color="yellow",  bold=True))
print(terminal_color("The terminal_color() function.", color="green",   bold=True))
print(terminal_color("The terminal_color() function.", color="red",     bold=True))
print(terminal_color("The terminal_color() function.", color="black",   bold=True))
#Different color values without bold
# white,cyan,purple (or magenta),blue,yellow,green,red,black
print(terminal_color("The terminal_color() function.", color="white",   bold=False))
print(terminal_color("The terminal_color() function.", color="cyan",    bold=False))
print(terminal_color("The terminal_color() function.", color="purple",  bold=False))
print(terminal_color("The terminal_color() function.", color="magenta", bold=False))
print(terminal_color("The terminal_color() function.", color="blue",    bold=False))
print(terminal_color("The terminal_color() function.", color="yellow",  bold=False))
print(terminal_color("The terminal_color() function.", color="green",   bold=False))
print(terminal_color("The terminal_color() function.", color="red",     bold=False))
print(terminal_color("The terminal_color() function.", color="black",   bold=False))



# III. Testing the terminal_font() function
#---------------------------------------------
print("Hello World, default text")
print(terminal_font("All defaults"))
print(terminal_font("Black background, bold underlined white font", fontcolor="white", 
	bgcolor="black", bold=True, underlined=True))
print(terminal_font("Red background, underlined blue font", fontcolor="blue", 
	bgcolor="red", underlined=True))
print(terminal_font("Yellow bold font", fontcolor="yellow", bold=True))
print(terminal_font("Black underlined font", fontcolor="black", underlined=True))
print(terminal_font("White bold font, green background", fontcolor="white", bgcolor="green"))
'''
















