#!/usr/bin/python3

# The purpose of this code is to cheat at wordl.net

#The code works in a linux terminal in the following manner.
# 1) Ingest a complete English lexicon file, turn the contents into a list.
#(This is a copy from vocabulary_estimator_en.py)
# 2) Collect information from the user regarding what is known about letters so far.

# LIBRARIES
#===================================================================
import time
import random
import math
from tutor_functions import terminal_font

#---------------------------------------------
# Simple method for cycling through lines:
# https://www.geeksforgeeks.org/read-a-file-line-by-line-in-python/
# Opening file
words_alpha = open('words_alpha.txt', 'r')
words = []
# Using for loop
for line in words_alpha:
	words.append(line.strip())
	#TEST
	#print(len(words))
	#print(words[count])
	#count += 1
	#END TEST
# Closing files
words_alpha.close()
#convert all letters to uppercase:
words = [x.upper() for x in words]
#TEST
#test = words[301000:301115]
#print(test)
#END TEST


#Wordle words from
#https://github.com/steve-kasica/wordle-words/blob/master/wordle.csv
#This wordle word list also has some information about frequency?
words_wordl_file = open('words_wordle.txt', 'r')
words_wordl = []
for line in words_wordl_file:
	words_wordl.append(line.strip())
words_wordl_file.close()
#convert all letters to uppercase:
words_wordl = [x.upper() for x in words_wordl]


#Ask the user to input "word length"
length = input("Enter the word length:\n(Integer)\n")
#Ask the user to input "placed letters"
placed = input("Placed letters:\n(In the format _ _ _ A _ where A is the letter A at position 4 (1-5)\n")
#Ask the user to input "misplaced letters"
misplaced = input("Misplaced letters:\n(In the format _ AB A _ _ where A is the letter A at position 2 & 3, and B is the letter B at position 3 (1-5)\n")
#Ask the user to add input "incorrect letters"
incorrect = input("Incorrect letters:\n(In the format C D I M where C,D,I, & M are letters that are not in the unknown word\n")
#Are there any letters that are known to appear twice, but the 2nd is misplaced?
#Or where a second copy of the letter is known *not* to be in the word?
#These data are currently not accounted for. 
misplaced2nd = ""
incorrect2nd = ""

#HARD CODING IN ANSWERS

length = 5
placed = "_ _ _ _ _"
misplaced = "_ _ _ _ _"
incorrect = "B O R E D F A I T H C L U N K"

length = 5
placed = "D _ _ _ _"
misplaced = "_ O _ E ET"
incorrect = "C U P S L U N G V W L Y R A F"

length = 5
placed = "D E T O X"
misplaced = "_ _ _ _ _"
incorrect = ""

length = 8
placed = "_ _ _ _ _ _ _ _"
misplaced = "T R REU AOK EO B _ ET"
incorrect = "Y I P S D G H J L C N M"

length = 5
placed = "_ _ _ _ E"
misplaced = "_ UR R RV _"
incorrect = "I A T S H O P M C"

length = 5
placed = "_ _ _ _ _"
misplaced = "_ _ _ _ _"
incorrect = "B O R E D F A I T H C L U N K"

length = 5
placed = "_ E _ _ _"
misplaced = "_ LY N NE _"
incorrect = "B O R D C U K F A I T H G M P S V"

length = 5
placed = "_ P _ _ _"
misplaced = "_ _ _ _ T"
incorrect = "S H O F C N I E"

length = 5
placed = "D _ _ _ E"
misplaced = "_ O I E D"
incorrect = "B L A S T C R N U F P W G"

length = 5
placed = "_ _ _ _ _"
misplaced = "T _ UI N P"
incorrect = "A E D C G Y H B M L"

length = 5
placed = "_ _ _ _ _"
misplaced = "N LY LI N Y"
incorrect = "W E R T U O P A S D F G H C B M"

length = 5
placed = "_ _ _ E _"
misplaced = "CT _ O _ _"
incorrect = "W R Y U I P A S D F G H L B N M"

length = 5
placed = "_ _ _ _ _"
misplaced = "W L L EW S"
incorrect = "R T Y U I O P A D F G H C B N M"

length = 5
placed = "_ _ U _ _"
misplaced = "_ AL _ A L"
incorrect = "Q W E R T Y I O P S D F G H C B N M"

length = 5
placed = "_ _ _ _ _"
misplaced = "_ R IO _ S"
incorrect = "W E T Y U P A D F G H L C B N M"

length = 5
placed = "_ I _ _ _"
misplaced = "_ H IO _ P"
incorrect = "W E R T Y U A S D F G K L C B N M"

length = 5
placed = "_ _ _ E _"
misplaced = "P UR UR _ PE"
incorrect = "W T Y I O A S D F G H L C B N M"

#ADERST
length = 5
placed = "_ _ _ _ _"
misplaced = "_ _ _ _ _"
incorrect = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
incorrect = "B C F G H I J K L M N O P Q U V W X Y Z"


#ADERST
length = 5
placed = "_ _ _ _ _"
misplaced = "_ RU UE E R"
incorrect = "Q W T Y I O P A S D F G H L C B N M"

length = 5
placed = "_ O _ _ _"
misplaced = "N HR OR N H"
incorrect = "W E T Y U I P A S D F G L C B M"

length = 5
placed = "_ _ _ _ E"
misplaced = "D I D I _"
incorrect = "W R T Y O P A S H K L C M"

# 2nd Es



#Pre-processing the input
placed_list = placed.upper().split(" ")
misplaced_list = misplaced.upper().split(" ")
incorrect_list = incorrect.upper().split(" ")


#Function to determine if word conforms
def valid_word(word, length, placed_list, misplaced_list, incorrect_list):
  #If the word isn't the right length, just return False
  if length != len(word):
    valid = False
    return valid
  word_letters = list(word)
  placed_bool    = False
  misplaced_bool = False
  incorrect_bool = False
  length_bool    = False
  #Is the word the correct length?
  #THIS IS NOW REDUNDANT, but whatever.
  #--------------------------------
  if len(word_letters) == length:
    length_bool = True
  else:
    pass
  #--------------------------------
  #Does the word conform to placed?
  mybools = []
  for i, j in enumerate(placed_list):
    if j != "_":
      #Check the ith word letter
      if j == word_letters[i]:
        mybools.append(True)
      elif j != word_letters[i]:
        mybools.append(False)
    placed_bool = all(mybools)
  #--------------------------------
  #Does the word conform to misplaced?
  #This is actually two pieces of logic
  # A) Does the word have the misplaced letters?
  # B) Are the letters in the specific wrong place?
  #ORIGINAL, JUST MAKE TRUE LINE:
  #misplaced_bool = True
  A_bools = []
  B_bools = []
  #A
  for i in misplaced_list:
    for l in i:
      if l != "_":
        x = 0 < word.count(l)
        A_bools.append(x)
  #B
  for n, i in enumerate(misplaced_list):
    for l in i:
      if l != "_":
        #HERE
        #TEST
        #print("len(word_letters): ", len(word_letters))
        #print(word_letters)
        #END TEST
        if l == word_letters[n]:
          B_bools.append(False)
        elif l != word_letters[n]:
          B_bools.append(True)
  #Collapse the logic of A & B into AB
  AB_bools = A_bools + B_bools
  misplaced_bool = all(AB_bools)
  #--------------------------------
  #Does the word conform to incorrect?
  #Confusingly, incorrect_bool == True means it is not incorrect.
  I_bools = []
  for w in word_letters:
    for i in incorrect_list:
      if i == w:
        I_bools.append(False)
  incorrect_bool = all(I_bools)
  #--------------------------------
  #Is the word valid?
  if placed_bool == True and misplaced_bool == True and incorrect_bool == True and length_bool == True: 
    valid = True
  else:
    valid = False
  return valid



#TEST THE FUNCTION:
#myword = "BEARS"
#print(valid_word(myword, length, placed_list, misplaced_list, incorrect_list))
#END TEST

#RUN THROUGH WORDS
filtered = []
for w in words:
  if valid_word(w, length, placed_list, misplaced_list, incorrect_list):
    filtered.append(w)
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("Larger dictionary words:\n", filtered)
print("\n")

#RUN THROUGH words_wordl
filtered = []
for w in words_wordl:
  if valid_word(w, length, placed_list, misplaced_list, incorrect_list):
    filtered.append(w)

print("Wordle words:")
for f in filtered:
  print(terminal_font(f, fontcolor="blue", bgcolor="none", bold=True, underlined=False), end =", ")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")
print("\n")














