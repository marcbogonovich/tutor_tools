#!/usr/bin/python3

# The actual corpora is located here:
# If you go to dl the actual corpora, it appears through that link,
# that there are several other resources, including a generic word
# commonality rank list, up to 60,000 words. And for some reason, this
# isn't listed elsewhere. With a 20,000 common word list included instead
# which has clunky inclusion criteria. And you find this more natural 
# 60,000 word list when trying to dl the whole corpus (not when looking 
# for word commonality lists). And there isn't an explicit link for this 
# page that I can store here, instead you need to click on the below link,
# and then CLICK ON THE GREEN DOWN BUTTON AT THE TOP OF THE PAGE (NOT the 
# same green down button located in a green bar lower in the page, which is
# a dead link), and then this presents the resources.
# https://www.english-corpora.org/coca/
#
# If you do eventually click on the resource for the 60,000 most frequent
# words, it takes you here:
# https://www.wordfrequency.info/
#
# And HERE IS WHERE THEY TELL YOU, THAT YOU CAN PURCHASE THE DATA. They did
# not make it easy to find, it would be nearly easier to just recalculate it 
# with a script than to find it and then pay for it.
#
# https://www.academicwords.info/
